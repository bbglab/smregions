.. _readme:

SMRegions
=========

.. warning::
    The smRegions repository on Bitbucket is currently in **read-only mode**. This means that no further updates, issues, or pull requests will be processed on Bitbucket. 

    For the latest updates, new releases, or to contribute to the project, please refer to the official `smRegions repository on GitHub <https://github.com/bbglab/smregions>`_. All future development and maintenance will be conducted on GitHub.

.. |sr| replace:: **SMRegions**


.. TODO


Usage
-----

|sr| can be used through the command line interface.
Use ``-h`` or ``--help`` to access the help information.


.. _readme install:

Installation
------------

To install this package, clone the repo and install it with pip.


.. _readme license:

License
-------

This software is released under Apache Software License 2.0

